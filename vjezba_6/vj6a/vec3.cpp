#include "vec3.h"
#include <iostream>
#include <math.h>

using namespace std;
using namespace oop;

int Vec3::_counter=0;
Vec3::Vec3()
{
    _counter++;
}
Vec3::Vec3(double x=0, double y=0, double z=0)
{
    _x=x;
    _y=y;
    _z=z;
    _counter++;
}
Vec3::~Vec3()
{
    _counter--;
}
double Vec3::getX()
{
    return _x;
}
double Vec3::getY()
{
    return _y;
}
double Vec3::getZ()
{
    return _z;
}

Vec3 Vec3::operator+(Vec3& other)
{
    Vec3 obj(_x + other._x, _y + other._y, _z + other._z);
    return obj;
}
Vec3 Vec3::operator-(Vec3& other)
{
    return Vec3(_x - other._x, _y - other._y, _z - other._z);
}

Vec3 Vec3::operator*(int skalar)
{
    return Vec3(_x*skalar, _y*skalar, _z*skalar);
}
Vec3 Vec3::operator/(int skalar)
{
    return Vec3(_x/skalar, _y/skalar, _z/skalar);
}


void Vec3::operator+=(int n)
{
    _x+=n;
    _y+=n;
    _z+=n;
}
void Vec3::operator-=(int n)
{
    _x-=n;
    _y-=n;
    _z-=n;
}
void Vec3::operator*=(int n)
{
    _x*=n;
    _y*=n;
    _z*=n;
}
void Vec3::operator/=(int n)
{
    _x/=n;
    _y/=n;
    _z/=n;
}
double Vec3::operator*(Vec3 &other)
{
    return (_x*other._x + _y*other._y + _z*other._z);
}
Vec3 Vec3::operator=(const Vec3& other)
{
    _x=other._x;
    _y=other._y;
    _z=other._z;

    return *this;
}


bool Vec3::operator==(Vec3 &other)
{
    return(_x==other._x && _y==other._y && _z==other._z);
}
bool Vec3::operator!=(Vec3 &other)
{
    return(_x!=other._x && _y!=other._y && _z!=other._z);
}
bool Vec3::operator>(Vec3 &other)
{
    return(_x>other._x && _y>other._y && _z>other._z);
}
bool Vec3::operator<(Vec3 &other)
{
    return(_x<other._x && _y<other._y && _z<other._z);
}
bool Vec3::operator>=(Vec3 &other)
{
    return(_x>=other._x && _y>=other._y && _z>=other._z);
}
bool Vec3::operator<=(Vec3 &other)
{
    return(_x<=other._x && _y<=other._y && _z<=other._z);
}

double Vec3::operator[](unsigned int i){
    if (i==0)
        return _x;
    if (i==1)
        return _y;
    if (i==2)
        return _z;
}

void Vec3::normalization(){
    _x=_x/(sqrt(pow(_x,2)+pow(_y,2)+pow(_z,2)));
    _y=_y/(sqrt(pow(_x,2)+pow(_y,2)+pow(_z,2)));
    _z=_z/(sqrt(pow(_x,2)+pow(_y,2)+pow(_z,2)));
}

void Vec3::printCounter(){
    cout<<_counter<<endl;
}
