#ifndef VEC3_H
#define VEC3_H
#include <iostream>
#include <string>

using namespace std;

namespace oop{
    class Vec3
    {
        public:
            Vec3();
            Vec3(double x, double y, double z);
            ~Vec3();
            double getX();
            double getY();
            double getZ();

            Vec3 operator+(Vec3 &other);
            Vec3 operator-(Vec3 &other);
            Vec3 operator=(const Vec3 &other);
            Vec3 operator*(int skalar);
            Vec3 operator/(int skalar);
            void operator+=(int n);
            void operator-=(int n);
            void operator*=(int n);
            void operator/=(int n);

            double operator*(Vec3 &other);

            bool operator ==(Vec3& other);
            bool operator !=(Vec3& other);
            bool operator >(Vec3& other);
            bool operator <(Vec3& other);
            bool operator >=(Vec3& other);
            bool operator <=(Vec3& other);

            double operator [](unsigned int i);

            void normalization();
            void printCounter();

            friend ostream & operator << (ostream &os, Vec3 &p){
                os << "(" << p._x << "," << p._y << "," << p._z<<")";
                return os;
            }
            friend istream & operator >>(istream &is , Vec3 &p) {
                cout<<"x: ";
                is >>p._x;
                cout<<"y: ";
                is >>p._y;
                cout<<"z: ";
                is >> p._z;
                return is ;
            }
        private:
            double _x,_y,_z;
            static int _counter;

    };
}


#endif // VEC3_H
