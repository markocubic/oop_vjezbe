#include "array.h"
#include <iostream>
#include <math.h>
#include <time.h>
#include <random>
#include <cstdio>

using namespace std;
using namespace oop;

Array::Array()
{
    _siz=0;
    _niz=new int;
}
Array::Array(unsigned int s)
{
    _siz=s;
    _niz=new int[_siz];
}
Array::Array(unsigned int s, int *niz)
{
    _siz=s;
    delete [] _niz;
    _niz= new int[_siz];

    for(unsigned int k=0; k<_siz; k++)
    {
        _niz[k]=niz[k];
    }

    cout<<"print niz: "<<endl;
    for(unsigned int k=0; k<s; k++)
    {
        cout<<niz[k]<<" ";
    }
    cout<<endl;
}

Array::~Array()
{
    delete[] _niz;
}

void Array::randomFillArray()
{
    for (unsigned int i=0;i<_siz;i++)
        _niz[i]=(rand()%100)+1;
}

void Array::printArray() const
{
    for (unsigned int i=0;i<_siz;i++)
    {
        cout<<_niz[i]<<" ";
    }
    cout<<endl;
}
Array& Array::operator=(const Array &other)
{
    if (this != &other)
    {
        _siz=other._siz;
        _niz=other._niz;
    }
    return *this;
}
const Array Array::operator+(const Array &other)
{
    Array obj(_siz + other._siz);

    for(unsigned int i=0; i<_siz; i++)
        obj._niz[i]=_niz[i];
    for(unsigned int j=0; j<other._siz; j++)
        obj._niz[j+_siz]=other._niz[j];

    obj.printArray();
    return obj;//u ovom returnu se izgube elementi
}

Array Array::operator-(const Array &other)
{
    Array obj(_siz);
    for(unsigned int i=0; i<_siz; i++)
    {
        obj._niz[i]=_niz[i]-other._niz[i];
    }
    return obj;
}
namespace oop{
    ostream& operator<<(ostream &os, Array &p)
    {
        for (unsigned int i=0;i<p._siz;i++)
            os<<p._niz[i]<<" ";

        return os;
    }
    istream& operator >>(istream &is, Array &p){
        if(p._siz!=0){
            cout<<"\nUnesite "<<p._siz<<" brojeva."<<endl;
            for (unsigned int i=0;i<p._siz;i++)
            {
                cout<<"Unesi "<<i+1<<". broj:";
                is>>p._niz[i];
            }
            return is;
        }
        else{
            cout<<"Niz je prazan."<<endl;//Opcija za odredit velicinu niza
        }
        return is;
    }
}
