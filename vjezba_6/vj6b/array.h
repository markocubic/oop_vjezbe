#ifndef ARRAY_H
#define ARRAY_H
#include <iostream>

using namespace std;
namespace oop{
    class Array
    {
        public:
            Array();
            Array(unsigned int _siz);
            Array(unsigned int _siz, int *_niz);
            ~Array();
            void randomFillArray();
            void printArray() const;
            friend ostream& operator<<(ostream &os, Array &p);
            friend istream & operator >>(istream &is , Array &p);

            Array& operator=(const Array &other);
            const Array operator+(const Array &other);
            Array operator-(const Array &other);

        private:
            int *_niz;
            unsigned int _siz;
    };
}

#endif
