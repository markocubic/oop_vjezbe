#include <iostream>
#include <time.h>
#include <random>
#include "array.h"
using namespace std;
using namespace oop;

int main()
{
    srand (time(NULL));
    Array niz1(3);
    Array niz2(3);
    Array niz3;
    niz1.randomFillArray();
    niz2.randomFillArray();
    /*PROVJERA ZA x+y
    cout<<niz1<<endl;
    cout<<niz2<<endl;
    niz2=niz1+niz2;
    //cout<<niz1<<endl;
    //cout<<niz2<<endl;
    cout<<niz2;
    */

    cout<<niz1<<endl;
    cout<<niz2<<endl;

    niz3=niz1+niz2;
    cout<<niz3<<endl;
}
