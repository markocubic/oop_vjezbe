#include <iostream>
#include "stack.h"
using namespace std;

int main()
{
    Stack<int> s;

    s.push(5);
    cout<<s.get_top()<<endl;
    cout<<s.get_size()<<endl;
    s.pop();
}
