#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <vector>

using namespace std;

template <typename T>
class Stack
{
    private:
        const int _defaultSize=10;
        const int _maxSize=1000;
        int _size;
        int _top;
        T* _stackPtr;
        vector<T> _v;

    public:
        Stack(){
            cout<<"Stack Konstruktor"<<endl;
            _stackPtr=&_v.back();
        }

        ~Stack(){};

        void push(T a)
        {
            if(checkStack()){
                _v.push_back(a);
                _stackPtr=&_v.back();
                cout<<"push "<<a<<endl;
            }
            else
                cout<<"Stack je pun"<<endl;
        }

        T pop()
        {
            cout<<"pop "<<_v.at(_v.size()-1)<<endl;
           _v.pop_back();
           _stackPtr=&_v.back();
        }

        bool checkStack()
        {
            if(_v.size()==0){
                cout<<"Stack je prazan..."<<endl;
                return true;
            }
            else if(_v.size()==1000){
                cout<<"Stack je popunjen!"<<endl;
                return false;
            }
            return true;
        }

        int get_size(){
            return _v.size();
        }
        int get_top(){
            if(checkStack()){
                _top=_v.at(_v.size()-1);
                return _top;
            }
        }
};

#endif // STACK_H
