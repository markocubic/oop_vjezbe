#ifndef ZBROJ_H
#define ZBROJ_H
#include <iostream>

using namespace std;

template <typename T>
class Zbroj
{
    private:
        T rezultat;
    public:
        T zbroj(T a, T b){
            return a+b;
        }
        Zbroj(){
            cout<<"Konstruktor Obicna"<<endl;
        }
};

template <>
class Zbroj<char>
{
    private:
        char rezultat;
    public:
        char zbroj(char a, char b)
        {
            if(isalpha(a)&& isalpha(b))
                rezultat=(a-'0')+(b-'0');
            else
                rezultat=(a-'0')+(b-'0')+'0';
            return rezultat;
        }
        Zbroj(){
            cout<<"Konstruktor Specijalizacijska"<<endl;
        }
};

#endif // ZBROJ_H
