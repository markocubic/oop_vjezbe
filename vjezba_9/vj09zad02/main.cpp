#include "zbroj.h"
#include <iostream>

using namespace std;

int main()
{
    Zbroj<int> zbr1;
    cout<<zbr1.zbroj(7,8)<<endl;
    Zbroj<char> zbr2;
    cout<<zbr2.zbroj('6','3')<<endl;
}
