#include <iostream>
#include <random>
#include <time.h>
#include "Point.h"

using namespace std;

void Point::setDouble(double x, double y, double z)
{
    duzina=x;
    sirina=y;
    visina=z;
}

void Point::randDouble()
{
    duzina = rand() % 100 + 1;
    sirina = rand() % 100 + 1;
    visina = rand() % 100 + 1;
}

double Point::calculateDistanceFrom2D(Point point)
{
    return sqrt(pow((point.duzina - duzina), 2) + pow((point.sirina - sirina), 2));
}
double Point::calculateDistanceFrom3D(Point point)
{
    return sqrt(pow((point.duzina - duzina), 2) + pow((point.sirina - sirina), 2) + pow((point.visina - visina), 2));
}

double Point::getDuzina()
{
    return duzina;
}
double Point::getSirina()
{
    return sirina;
}
double Point::getVisina()
{
    return visina;
}
