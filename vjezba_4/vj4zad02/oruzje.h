#ifndef ORUZJE_H
#define ORUZJE_H
#include "Point.h"
using namespace std;

class Oruzje
{
    public:
        void setOruzje();
        void shoot();
        void reload();
        int getSanzer();
        int getHeight();
    private:
        Point point;
        int pun_sanzer, stanje_sanzer;
};

#endif // ORUZJE_H
