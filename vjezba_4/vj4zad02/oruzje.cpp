#include <iostream>
#include <time.h>
#include <random>
#include "oruzje.h"
#include "Point.h"
using namespace std;
//Oruzje

void Oruzje::setOruzje()
{
    point.setDouble(7,-1,0);
    pun_sanzer=10;
    stanje_sanzer=pun_sanzer;
}
void Oruzje::reload()
{
    stanje_sanzer=pun_sanzer;
}
void Oruzje::shoot()
{
    if(stanje_sanzer==0)
        reload();
    stanje_sanzer=stanje_sanzer-1;
}
int Oruzje::getSanzer()
{
    return stanje_sanzer;
}
int Oruzje::getHeight()
{
    return point.getVisina();
}
