#include <iostream>
#include <random>
#include <time.h>
#include "Point.h"

using namespace std;

int main()
{
    srand(time(NULL));

    Point p1;
    Point p2;
    p1.setDouble(10.0,2.2,3.8);
    p2.randDouble();

    cout<<"duzina: "<<p1.getDuzina()<<endl;
    cout<<"sirina: "<<p1.getSirina()<<endl;
    cout<<"visina: "<<p1.getVisina()<<endl;

    cout<<"rand duzina: "<<p2.getDuzina()<<endl;
    cout<<"rand sirina: "<<p2.getSirina()<<endl;
    cout<<"rand visina: "<<p2.getVisina()<<endl;

    cout<<p1.calculateDistanceFrom2D(p2)<<endl;
    cout<<p1.calculateDistanceFrom3D(p2)<<endl;
}
