#ifndef POINT_H
#define POINT_H
#include <iostream>
#include <random>
using namespace std;

class Point{
public:

    void setDouble(double duzina = 0, double sirina = 0, double visina = 0);
    void randDouble();
    double calculateDistanceFrom2D(Point point);
    double calculateDistanceFrom3D(Point point);
    double getDuzina();
    double getSirina();
    double getVisina();

private:
    double duzina, sirina, visina;
};

#endif
