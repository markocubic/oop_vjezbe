#include <iostream>
#include <time.h>
#include <random>
#include "meta.h"
#include "Point.h"
using namespace std;

int main()
{
    srand(time(NULL));

    //Unesite broj meta
    int n=3;
    Meta niz[n];
    for(int i=0;i<n;i++)
    {
        niz[i].setMeta();
        cout<<"meta "<<i<<endl;
        cout<<"s: "<<niz[i].getSirina()<<endl;
        cout<<"v: "<<niz[i].getVisina()<<endl;
    }
}
