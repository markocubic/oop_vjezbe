#ifndef META_H
#define META_H
#include "Point.h"

class Meta
{
    public:
        void setMeta();
        double getSirina();
        double getVisina();

    private:
        int duzina, sirina, visina;
        Point donja_lijeva;
};

#endif // META_H
