#include <iostream>
#include <time.h>
#include <random>
#include "../vj4zad01/Point.h"
#include "../vj4zad02/oruzje.h"
#include "meta.h"
using namespace std;

int main()
{
    srand(time(NULL));

    Oruzje oruzje;
    oruzje.setOruzje();
    int n=3;//korisnik unosi
    Meta niz[n];//u vektor
    for (int i=0;i<n;i++)
    {
        oruzje.shoot();
        niz[i].setMeta();
        if (niz[i].getDonjaLijevaVisina()<oruzje.getHeight() && niz[i].getVisina()>oruzje.getHeight())
            niz[i].setPogodena();

    }
    for (int i=0;i<n;i++)
    {
        cout<<"Meta "<<i<<": "<<endl;
        cout<<" donja lijeva: "<<niz[i].getDonjaLijevaVisina()<<endl;
        cout<<"       visina: "<<niz[i].getVisina()<<endl;
        cout<<"       Status: ";
        (niz[i].getPogodena()) ? (cout<<"Pogodena"<<endl) : (cout<<"Promasena"<<endl);
    }
    cout<<"Broj metaka: "<<oruzje.getSanzer();
}
