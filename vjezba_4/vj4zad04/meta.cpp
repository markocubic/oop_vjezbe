#include "meta.h"
#include <iostream>
#include <time.h>
#include <random>
#include "../vj4zad02/oruzje.h"
#include "../vj4zad01/Point.h"


double Meta::getSirina()
{
    return sirina;
}
double Meta::getVisina()
{
    return visina;
}
double Meta::getDonjaLijevaVisina()
{
    return donja_lijeva.getVisina();
}
void Meta::setPogodena()
{
    pogodena=1;
}
bool Meta::getPogodena()
{
    return pogodena;
}
void Meta::setMeta()
{
    donja_lijeva.randDouble();
    sirina = rand() % 20 + 1;
    visina = getDonjaLijevaVisina()+5;
    pogodena=0;
}
