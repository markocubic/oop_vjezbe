#ifndef META_H
#define META_H
#include "../vj4zad01/Point.h"
#include "../vj4zad02/oruzje.h"

class Meta
{
    public:
        void setMeta();
        double getSirina();
        double getVisina();
        double getDonjaLijevaVisina();
        void setPogodena();
        bool getPogodena();

    private:
        bool pogodena;
        int duzina, sirina, visina;
        Point donja_lijeva;
};

#endif // META_H
