#include <iostream>
#include "oper.h"
using namespace std;

int main()
{
    Oper o;
    WriteLog wl;
    try{
        int a=o.unesiInt();
        int b=o.unesiInt();
        char c=o.operacija();
        o.rjesenje(a, b, c);
    }
    catch(exception& exc){
        wl.write(exc.what());
        cout<<exc.what()<<endl;
    }
}
