#ifndef OPER_H
#define OPER_H
#include <iostream>
#include <exception>
#include <fstream>
#include <ctime>

using namespace std;

class XOperand : public exception{
    public:
        virtual const char* what() const throw(){
            return "Krivi operand!";
        }
};
class XOperation : public exception{
    public:
        virtual const char* what() const throw(){
            return "Krivi operator!";
        }
};
class XDivideByZero : public exception{
    public:
        virtual const char* what() const throw(){
            return "Dijeljenje s nulom!";
        }
};

class WriteLog
{
    public:
        void write(string message){
            ofstream fout("errors.log", ios_base::out | ios_base::app);
            time_t rawtime;
            time (&rawtime);
            struct tm * timeinfo;
            char buffer[100];
            timeinfo = localtime(&rawtime);
            strftime(buffer, sizeof(buffer), "%d-%m-%y %H:%M:%S", timeinfo);
            string str(buffer);
            fout<<str<<" ";
            fout<<message<<endl;
            fout.close();
        }
};

class Oper
{
    public:
        int unesiInt(){
            cout<<"Unesite int: ";
            if(!(cin>>m_a))
                throw XOperand();
            else
                return m_a;
        }
        char operacija(){
            cout<<"Unesite operator: ";
            cin>>m_b;
            if(m_b != '+' && m_b != '-' && m_b != '*' && m_b != '/')
                throw XOperation();
            return m_b;
        }
        void rjesenje(int a, int b, char operand)
        {
            if(operand == '+')
                cout<<" Zbroj je: "<<a + b<<endl;
            else if (operand == '-')
                cout<<" Razlika je: "<<a - b<<endl;
            else if (operand == '*')
                 cout<<" Umnozak je: "<<a * b << endl;
            else if(operand == '/'){
                if (b == 0)
                    throw XDivideByZero();
                else
                    cout<<" Koli�nik je: "<<a / b<<endl;
            }
        }

    private:
        int m_a;
        char m_b;
};

#endif // OPER_H
