#include <iostream>
#include <vector>
#include <iterator>
#include <time.h>
#include <random>
using namespace std;

void ispis_poteza(vector<int> v, int &potez)
{
    int br=0;
    for (int i=0;i<potez;i++)
    {
        if(i==0 || i%2==0)
        {
            br++;
            cout<<"\nPotez "<<br<<"\n";
            cout<<"   NPC: "<<v[i]<<"\n";
        }
        else
            cout<<" Igrac: "<<v[i]<<"\n";
    }
}

void menu()
{
    while(true)
    {
        int flagf;
        cout<<"\n0 za zapoceti igru";
        cout<<"\n1 za instrukcije";
        cout<<"\nUnos: ";
        cin>>flagf;
        if(flagf==0)
            return;

        else if(flagf==1)
        {
            cout<<"\n Instrukcije:";
            cout<<"\n  Pred 2 igraca postavi se 21 sibica.";
            cout<<"\n  Igraci se izmjenjuju i uklanjaju 1, 2 ili 3 sibice odjednom.";
            cout<<"\n  Igrac koji je prisiljen uzeti posljednju sibicu gubi.\n";
        }
    }
}

void tijek_igre(vector<int> &v, int &potez)
{
    int uk_br=21, moj_br, npc_br;
    srand(time(NULL));

    cout<<"\nSTART\n";
    while(uk_br > 0)
    {
        //NPC
        for(int m=5;m>=0;m--)
        {
            if((uk_br-(m*4+1))<4 && (uk_br-(m*4+1))>0){
                npc_br=uk_br-(m*4+1);
                break;
            }
            else if(m==0){
                if (uk_br!=1)
                    npc_br=rand()%3+1;
                else
                    npc_br=1;
            }
        }
        uk_br=uk_br-npc_br;
        cout<<" \nProtivnik skida "<<npc_br;
        (npc_br==1)?(cout<<" sibicu"):(cout<<" sibice");
        v[potez]=npc_br;
        potez++;
        if(uk_br<=0){
            cout<<"\n\nPOBJEDILI STE\n";
            break;
        }

        //KORISNIK
        cout<<" \nUkupan broj sibica: "<<uk_br;
        cout<<"  \nUnesi broj: ";
        cin>>moj_br;
        while(moj_br>3 || moj_br<1)
        {
            cout<<"\n Kriv unos. Mozes unositi samo brojeve od 1 do 3!";
            cout<<"\nUnesi broj(1-3): ";
            cin>>moj_br;
        }
        while(moj_br>uk_br)
        {
            cout<<"\n Kriv unos. Ostalo je samo "<<uk_br<<" sibica!";
            cout<<"\nUnesi broj: ";
            cin>>moj_br;
        }
        uk_br=uk_br-moj_br;
        v[potez]=moj_br;
        potez++;
        if(uk_br<=0){
            cout<<"\n\nIZGUBILI STE\n";
            break;
        }
    }
}

void igra()
{
    cout<<" IGRA: Sibice\n\n";
    int flagf, potez=0;
    vector<int> v(21);
    menu();
    tijek_igre(v, potez);
    while(true)
    {
        cout<<"\n 0 - Ispisi poteze";
        cout<<"\n 1 - Izadi iz igre";
        cout<<"\n 2 - Restart\n";
        cout<<"\n Unos: ";
        cin>>flagf;
        if(flagf==0){
            ispis_poteza(v, potez);
        }
        else if(flagf==1){
            break;
        }
        else if(flagf==2){
            tijek_igre(v, potez);
        }
        else
            cout<<"\nKriv unos!\n";
    }
    cout<<"\n\nIgra zavrsena!";
}

int main()
{
    igra();
}
