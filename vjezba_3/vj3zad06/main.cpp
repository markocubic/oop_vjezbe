#include <iostream>
#include <vector>
#include <cctype>

using namespace std;

int letter_all_check(const string str)
{
    for(unsigned int i=0; i<str.size(); i++)
    {
        if (!isalpha(str[i]))
            return 0;
    }
    return 1;
}

int number_letter_check(const string str)
{
    for(unsigned int i=0; i<str.size(); i++)
    {
        if(!(isdigit(str[i]) || (isalpha(str[i]) && isupper(str[i]))))
            return 0;
    }
    return 1;
}

void print(const vector<string> &v)
{
    for (unsigned int i=0; i<v.size(); i++)
        cout<<v[i]<<endl;
}

void unos(vector<string> &v)
{
    int n;
    cout<<"Unesite broj rjeci koje cete unjeti: ";
    cin>>n;
    v.resize(n);
    for (int i=0; i<n; i++)
    {
        while(true)
        {
            cout<<"\nUnesi string "<<i+1<<": ";
            cin>>v[i];
            if(v[i].size()<20 && number_letter_check(v[i]))
                break;
            else
                cout<<"\nKriv unos!"<<endl;
        }
    }
}

void shift_delete(string &str, int i)
{
    for(i; i<str.size(); i++)
        str[i]=str[i+1];
    str.erase(str.size()-1);
}

void letterToNumber(string &str)
{
    int br=2;
    for(unsigned int i=0; i<str.size(); i++)
    {
        if(str[i]==str[i+1])
        {
            for (unsigned int j=i+1; j<str.size(); j++)
            {
                if(str[j]==str[j+1])
                {
                    br++;
                    shift_delete(str, j);
                    j--;
                }
                else{
                    str[i]='0'+br;
                    br=2;
                    break;
                }
            }
        }
    }
}

void numberToLetter(string &str)
{
    int br;
    for(unsigned int i=0; i<str.size(); i++)
    {
        if(isdigit(str[i]) && isalpha(str[i+1]))
        {
            br=str[i]-'0';
            str.insert(i+1, br-1, str[i+1]);
            shift_delete(str, i);
        }
    }
}
void transform_str_arr(vector<string> &v)
{

    for (unsigned int i=0; i<v.size(); i++)//seta po stringovima vektora
    {
        if(letter_all_check(v[i]))
            letterToNumber(v[i]);
        else
            numberToLetter(v[i]);
    }
}

int main()
{
    vector<string> v;
    unos(v);
    transform_str_arr(v);
    print(v);
}
