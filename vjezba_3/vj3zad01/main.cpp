#include <iostream>
#include <vector>
#include <time.h>
#include <cstdlib>
//#include <random>
using namespace std;

void print_vektora(vector<int> v, int n)
{
    cout<<"\nElementi vektora: \n";
    for (int i=0;i<n;i++)
    {
        cout<<v[i]<<" ";
    }
}

vector<int> unos_vektora(bool flag, int n=5, int a=0, int b=100)
{
    vector<int> v(n);
    int br;
    if (flag==0)
    {
        cout<<"Unosi elemente ("<<a<<" do "<<b<< ") u vektor: \n";
        for (int i=0;i<n;i++)
        {
            cin>>br;
            if (br>0 && br<100)
                v[i]=br;

            else if (br<0 || br>100)
            {
                cout<<"\nKrivo unesen broj!\n";
                i--;
            }
        }
        return v;
    }
    if (flag==1)
    {

        cout<<"Generiraju se random elementi ("<<a<<" do "<<b<< ") u vektor: \n";
        //default_random_engine generator;
        //uniform_int_distribution<int> distribution(a,b);

        int randn;
        srand(time(NULL));
        for (int i=0;i<n;i++)
        {
            //randn = distribution(generator);
            randn = rand()%100+1;
            v[i]=randn;
        }
        return v;
    }
}
int main()
{
    int n=5;//broj elemenata
    int a=0, b=100;//scope
    bool flag=0;
    vector<int> v=unos_vektora(flag, n, a, b);
    print_vektora(v, n);
}
