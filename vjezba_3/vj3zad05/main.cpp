#include <iostream>
#include <vector>
using namespace std;

struct producent{
    string name;
    string movie;
    int year;
};

struct producent_br{
    string name;
    int br=1;
};

void baza(vector<producent> &v)
{
    v.resize(7);
    v[0].name="Steven Spielberg";
    v[0].movie="Rat Svijetova";
    v[0].year=2005;

    v[1].name="Stanley Kubrick";
    v[1].movie="2001: A Space Odyssey";
    v[1].year=1969;

    v[2].name="Ridley Scott";
    v[2].movie="Bladerunner";
    v[2].year=1982;

    v[3].name="Steven Spielberg";
    v[3].movie="E.T.";
    v[3].year=1982;

    v[4].name="Ridley Scott";
    v[4].movie="Gladiator";
    v[4].year=2000;

    v[5].name="Ridley Scott";
    v[5].movie="Alien";
    v[5].year=1979;

    v[6].name="Steven Spielberg";
    v[6].movie="Schindlers List";
    v[6].year=1979;
}

void print_producent(const vector<producent> &v)
{
    for (int i=0;i<v.size();i++)
    {
        cout<<endl;
        cout<<"Producent "<<i+1<<": "<<endl;
        cout<<" "<<v[i].name<<endl;
        cout<<" "<<v[i].movie<<endl;
        cout<<" "<<v[i].year<<endl;
        cout<<endl;
    }
}

void unos_producent(vector<producent> &v)
{
    int br_p;
    cout<<"Koliko producenata zelite unjeti?"<<endl;
    cin>>br_p;
    v.resize(br_p);
    for (int i=0;i<br_p;i++)
    {
        cout<<endl;
        cout<<"Unesite ime producenta "<<i+1<<": ";
        cin>>v[i].name;
        cout<<"Unesite film producenta "<<i+1<<": ";
        cin>>v[i].movie;
        cout<<"Unesite godinu producenta "<<i+1<<": ";
        cin>>v[i].year;
    }
}

void fill_v(string str, vector<producent_br> &v1)
{
    for (int i=0;i<v1.size();i++)
    {
        if (v1[i].name==str)
        {
            v1[i].br++;
            return;
        }
    }
    v1.resize(v1.size()+1);
    v1[v1.size()-1].name=str;
}

void most_common(vector<producent> &v, vector<producent_br> &v1)
{
    for(int i=0;i<v.size();i++)
        fill_v(v[i].name, v1);
    int maxn=0;

    for(int i=0;i<v1.size();i++)
    {
        if(maxn<v1[i].br)
            maxn=v1[i].br;
    }
    cout<<"Najzastupljeniji producent/i: \n"<<endl;
    for(int i=0;i<v1.size();i++)
    {
        if(v1[i].br==maxn)
            cout<<" "<<v1[i].name<<endl;
    }
}

int main()
{
    vector<producent> v;
    vector<producent_br> v1;
    unos_producent(v);
    print_producent(v);
    //baza(v);

    most_common(v, v1);
}
