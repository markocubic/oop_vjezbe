#include <string>
#include <iostream>
#include "darksouls.h"

using namespace std;

string DarkSouls::getType()
{
    return _type;
}

string DarkSouls::getName()
{
    return _name;
}

vector<string> DarkSouls::platforms()
{
    return _platforms;
}

void DarkSouls::add(string str)
{
    _platforms.push_back(str);
}

void DarkSouls::check(string line)
{
    if (line.find("PC") != std::string::npos)
            add("PC");
    if (line.find("XBOX") != std::string::npos)
            add("XBOX");
    if (line.find("PS4") != std::string::npos)
            add("PS4");
}
