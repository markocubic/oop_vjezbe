#include <string>
#include <iostream>
#include "godofwar.h"

using namespace std;

string GodOfWar::getType()
{
    return _type;
}

string GodOfWar::getName()
{
    return _name;
}

vector<string> GodOfWar::platforms()
{
    return _platforms;
}

void GodOfWar::add(string str)
{
    _platforms.push_back(str);
}

void GodOfWar::check(string line)
{
    if (line.find("PC") != std::string::npos)
            add("PC");
    if (line.find("XBOX") != std::string::npos)
            add("XBOX");
    if (line.find("PS4") != std::string::npos)
            add("PS4");

}
