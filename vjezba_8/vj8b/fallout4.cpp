#include <string>
#include <iostream>
#include "fallout4.h"

using namespace std;

string Fallout4::getType()
{
    return _type;
}

string Fallout4::getName()
{
    return _name;
}

vector<string> Fallout4::platforms()
{
    return _platforms;
}

void Fallout4::add(string str)
{
    _platforms.push_back(str);
}

void Fallout4::check(string line)
{
    if (line.find("PC") != std::string::npos)
        add("PC");

    if (line.find("XBOX") != std::string::npos)
        add("XBOX");

    if (line.find("PS4") != std::string::npos)
        add("PS4");
}
