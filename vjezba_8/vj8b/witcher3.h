#ifndef WITCHER3_H
#define WITCHER3_H

#include "rpg.h"
#include "openworld.h"

using namespace std;
class Witcher3 : public RPG, public OpenWorld
{
    protected:
        vector<string> _platforms;
        string _name;
        string _type;
    public:
        Witcher3(){
            _type = "RPG and OpenWorld";
        }
        string getType();
        vector<string> platforms();
        void add(string s);
        string getName();
        void check(string line);
};

#endif // WITCHER3_H
