#ifndef GODOFWAR_H
#define GODOFWAR_H
#include <string>
#include "action.h"

class GodOfWar : public Action
{
    protected:
        vector<string> _platforms;
        string _name;
    public:
        string getType();
        vector<string> platforms();
        void add(string str);
        string getName();
        void check(string line);
};

#endif // GODOFWAR_H
