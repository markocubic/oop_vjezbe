#include "witcher3.h"
#include "openworld.h"

string  Witcher3::getType()
{
    return _type;
}

vector<string> Witcher3::platforms()
{
    return _platforms;
}

void Witcher3::add(string s)
{
    _platforms.push_back(s);
}

string Witcher3::getName()
{
    return _name;
}

void Witcher3::check(string line)
{
    if (line.find("PC") != std::string::npos)
            add("PC");
    if (line.find("XBOX") != std::string::npos)
            add("XBOX");
    if (line.find("PS4") != std::string::npos)
            add("PS4");
}
