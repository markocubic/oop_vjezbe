#include <iostream>
#include <fstream>
#include "counter.h"
#include "videogame.h"
    #include "action.h"
        #include "lastofus2.h"
        #include "godofwar.h"
    #include "rpg.h"
        #include "darksouls.h"
        #include "fallout4.h"
    #include "openworld.h"
        #include "witcher3.h"
using namespace std;

int main()
{
    VideoGame* h[]={new DarkSouls, new Fallout4, new LastOfUs2, new GodOfWar, new Witcher3}; //ide jos
    ifstream file("Igrice.txt");
    string line;
    while (getline(file, line)){
        string token = line.substr(0, line.find('#'));

        if(token=="DarkSouls3")
            h[0]->check(line);

        if(token=="Fallout4")
            h[1]->check(line);

        if(token=="LastOfUs2")
            h[2]->check(line);

        if(token=="GodOfWar")
            h[3]->check(line);

        if(token=="Witcher3")
            h[4]->check(line);
    }
    Counter c;
    for (int i=0; i<5; ++i){
        c.add(h[i]);
    }

    cout<<"Najzastupljenija platforma je ";
    c.printMostCommon();

}
