#ifndef LASTOFUS2_H
#define LASTOFUS2_H
#include <string>
#include "action.h"

class LastOfUs2 : public Action
{
    protected:
        vector<string> _platforms;
        string _name;
    public:
        string getType();
        vector<string> platforms();
        void add(string str);
        string getName();
        void check(string line);
};

#endif // LASTOFUS2_H
