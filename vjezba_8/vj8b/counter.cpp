#include "counter.h"
#include <iostream>

using namespace std;

Counter::Counter()
{
    _pc=0;
    _ps4=0;
    _xbox=0;
}

void Counter::add(VideoGame*& v)
{
    vector <string> k=v->platforms();
    vector <string>::iterator it;
    for(it=k.begin();it!=k.end();++it){
        if(*it=="PC")
            _pc+=1;
        if(*it=="PS4")
            _ps4+=1;
        if(*it=="XBOX")
            _xbox+=1;
    }
}

void Counter::printMostCommon() const
{
    if(_pc>_ps4 && _pc>_xbox)
        cout<<"PC"<<endl;

    if(_pc<_ps4 && _ps4>_xbox)
        cout<<"PS4"<<endl;

    if(_pc<_xbox && _ps4<_xbox)
        cout<<"XBOX"<<endl;
}
