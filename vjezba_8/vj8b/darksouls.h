#ifndef DARKSOULS_H
#define DARKSOULS_H
#include <vector>
#include "rpg.h"

class DarkSouls : public RPG
{
    protected:
        vector<string> _platforms;
        string _name;
    public:
        string getType();
        vector<string> platforms();
        void add(string str);
        string getName();
        void check(string line);
};

#endif // DARKSOULS_H
