#ifndef OPENWORLD_H
#define OPENWORLD_H
#include <vector>
#include <string>
#include "videogame.h"

using namespace std;

class OpenWorld : virtual public VideoGame
{
    public:
        virtual vector<string> platforms(){};
        string getType();
        virtual void check(string line){};
    protected:
        string _type="OpenWorld";


};

#endif // OPENWORLD_H
