#ifndef VIDEOGAME_H
#define VIDEOGAME_H
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class VideoGame
{
    public:
        virtual string getType() = 0;
        virtual ~VideoGame() = 0;
        virtual vector<string> platforms() = 0;
        virtual void check(string line) = 0;
};

#endif // VIDEOGAME_H
