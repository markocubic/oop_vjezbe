#ifndef FALLOUT4_H
#define FALLOUT4_H
#include <vector>
#include "rpg.h"

class Fallout4 : public RPG
{
    protected:
        vector<string> _platforms;
        string _name;
    public:
        string getType();
        vector<string> platforms();
        void add(string str);
        string getName();
        void check(string line);
};

#endif // FALLOUT4_H
