#ifndef COUNTER_H
#define COUNTER_H
#include "videogame.h"

class Counter
{
    private:
        int _pc;
        int _ps4;
        int _xbox;

    public:
        Counter();
        void add(VideoGame*& v);
        void printMostCommon() const;
};

#endif // COUNTER_H
