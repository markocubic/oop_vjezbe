#ifndef ACTION_H
#define ACTION_H
#include "videogame.h"
#include <vector>
#include <string>

using namespace std;

class Action : public VideoGame
{
    public:
        Action(){};
        virtual vector<string> platforms(){};
        string getType();
        virtual void check(string line){};
    protected:
        string _type="Action";
};

#endif // ACTION_H
