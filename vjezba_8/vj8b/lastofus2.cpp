#include <string>
#include <iostream>
#include "lastofus2.h"

using namespace std;

string LastOfUs2::getType()
{
    return _type;
}

string LastOfUs2::getName()
{
    return _name;
}

vector<string> LastOfUs2::platforms()
{
    return _platforms;
}

void LastOfUs2::add(string str)
{
    _platforms.push_back(str);
}

void LastOfUs2::check(string line)
{
    if (line.find("PC") != std::string::npos)
            add("PC");
    if (line.find("XBOX") != std::string::npos)
            add("XBOX");
    if (line.find("PS4") != std::string::npos)
            add("PS4");
}
