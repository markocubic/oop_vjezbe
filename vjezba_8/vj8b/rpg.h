#ifndef RPG_H
#define RPG_H
#include "videogame.h"
#include <vector>
#include <string>

class RPG : virtual public VideoGame
{
    public:
        virtual vector<string> platforms(){};
        string getType();
        virtual void check(string line){};
    protected:
        string _type="RPG";

};

#endif // RPG_H
