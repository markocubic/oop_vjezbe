#pragma once
#include "Mesni.h"

namespace oop {
	class Prsut :
		public Mesni
	{
	public:
		Prsut(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Prsut();
	};

}