#pragma once
#include "Mlijecni.h"

namespace oop {

	class Mlijeko :
		public Mlijecni
	{
	public:
		Mlijeko(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Mlijeko();
	};

}