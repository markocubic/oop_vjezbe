#pragma once
#include "Mlijecni.h"

namespace oop {

	class Sir :
		public Mlijecni
	{
	public:
		Sir(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Sir();
	};

}