#pragma once
#include "Vege.h"

namespace oop {

	class Hummus :
		public Vege
	{
	public:
		Hummus(float potrosnjaSamostalnogJela, float potrosnjaKaoPrilogUzJelo, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Hummus();
	};

}