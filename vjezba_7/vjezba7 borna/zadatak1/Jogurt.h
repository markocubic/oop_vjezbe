#pragma once
#include "Mlijecni.h"

namespace oop {

	class Jogurt :
		public Mlijecni
	{
	public:
		Jogurt(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Jogurt();
	};
}
