#pragma once
#include "Food.h"

using namespace std;

namespace oop {

	class Mesni :
		public Food
	{
	protected:
		float _potrosnjaSamostalnogJela;
		float _potrosnjaKaoDioDrugogJela;
	public:
		Mesni(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Mesni();
		virtual void print(std::ostream& os) const
		{
			os << "Naziv: " << _naziv << endl;
			os << "Potrosnja samostalnog jela: " << _potrosnjaSamostalnogJela << " " << "kg" << endl;
			os << "Potorsnja kao dio drugog jela: " << _potrosnjaKaoDioDrugogJela << " " << "kg" << endl;
		}
		friend istream & operator>>(istream & is, Mesni& m) {
			is >> m._potrosnjaSamostalnogJela
				>> m._potrosnjaKaoDioDrugogJela >> m._vrsta >> m._naziv
				>> m._kolicinaVoda >> m._protein >> m._mast
				>> m._ugljikohidrat >> m._rokTrajanja >> m._dnevnaKolicinaHrana;
			return is;
		}
	};

}