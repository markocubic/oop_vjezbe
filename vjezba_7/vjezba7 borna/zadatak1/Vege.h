#pragma once
#include "Food.h"
#include <string.h>

using namespace std;

namespace oop {

	class Vege :
		public Food
	{
	protected:
		float _potrosnjaSamostalnogJela;
		float _potrosnjaKaoPrilogUzJelo;
	public:
		Vege(float potrosnjaSamostalnogJela, float potrosnjaKaoPrilogUzJelo, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Vege();
		virtual void print(std::ostream& os) const
		{
			os << "Naziv: " << _naziv << endl;
			os << "Potrosnja samostalnog jela: " << _potrosnjaSamostalnogJela << " " << "kg" << endl;
			os << "Potorsnja kao prilog uz jelo: " << _potrosnjaKaoPrilogUzJelo << " " << "kg" << endl;
		}
		friend istream & operator>>(istream & is, Vege& m) {
			is >> m._potrosnjaSamostalnogJela
				>> m._potrosnjaKaoPrilogUzJelo >> m._vrsta >> m._naziv
				>> m._kolicinaVoda >> m._protein >> m._mast
				>> m._ugljikohidrat >> m._rokTrajanja >> m._dnevnaKolicinaHrana;
			return is;
		}
	};

}
