#pragma once
#include "Vege.h"

namespace oop {

	class Tofu :
		public Vege
	{
	public:
		Tofu(float potrosnjaSamostalnogJela, float potrosnjaKaoPrilogUzJelo, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Tofu();
	};

}