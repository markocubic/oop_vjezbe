#pragma once
#include "Mesni.h"

namespace oop {

	class Sunka :
		public Mesni
	{
	public:
		Sunka(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Sunka();
	};

}