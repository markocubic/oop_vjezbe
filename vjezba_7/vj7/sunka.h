#ifndef SUNKA_H
#define SUNKA_H

#include <iostream>
#include "mesni.h"

using namespace std;

namespace food{
class Sunka : public Mesni
{
    public:
        Sunka(double _potrosnja_sam, double _potrosnja_dio, string _vrsta, string _naziv, double _voda, double _protein,
                 double _mast, double _carb, string _rok, double _kolicinadan);

        friend ostream & operator<<(std::ostream &os, const Sunka& c){
            c.printMyself(os);
            return os;
        }
        friend istream & operator>>(istream & is, Sunka& c){
            is >> c._potrosnja_sam >> c._potrosnja_dio >> c._vrsta >> c._naziv >> c._voda >> c._protein >> c._mast
                    >> c._carb >> c._rok >> c._kolicinadan;
            return is;
       }

    protected:
        virtual void printMyself(ostream& os) const
        {
            os << "Naziv: " <<_naziv<< endl;
            os << "Potrosnja jela: " << _potrosnja_sam <<" "<<"kg"<<endl;
        }
};
}
#endif // SUNKA_H
