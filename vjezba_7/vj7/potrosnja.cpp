#include "potrosnja.h"

void Potrosnja::setYear(int y)
{
    _year=y;
}
void Potrosnja::setMonth(int m)
{
    _month=m;
}
void Potrosnja::setKg(double kg)
{
    _kg=kg;
}

int Potrosnja::getYear() const
{
    return _year;
}
int Potrosnja::getMonth() const
{
    return _month;
}
double Potrosnja::getKg() const
{
    return _kg;
}
