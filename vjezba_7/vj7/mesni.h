#ifndef MESNI_H
#define MESNI_H
#include "food.h"

using namespace food;

namespace food{
class Mesni : public Food
{
    public:
        Mesni(double _potrosnja_sam, double _potrosnja_dio, string _vrsta, string _naziv ,
              double _voda, double _protein, double _mast, double _carb, string _rok, double _kolicinadan);

        friend std::ostream & operator<<(std::ostream &os, const Mesni& o){
            o.printMyself(os);
            return os;
        }
        double get_kol(){
            return _potrosnja_sam;
        }

    protected:
        double _potrosnja_sam;
        double _potrosnja_dio;
        virtual void printMyself(std::ostream& os) const{}
};
}

#endif // MESNI_H
