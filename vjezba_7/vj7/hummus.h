#ifndef HUMMUS_H
#define HUMMUS_H

#include <iostream>
#include "vege.h"
using namespace std;

namespace food{
class Hummus : public Vege
{
    public:
        Hummus(double _potrosnja_sam, double _potrosnja_prilog, string _vrsta, string _naziv, double _voda, double _protein,
                 double _mast, double _carb, string _rok, double _kolicinadan);

        friend ostream & operator<<(std::ostream &os, const Hummus& c){
            c.printMyself(os);
            return os;
        }
        friend istream & operator>>(istream & is, Hummus& c){
            is >> c._potrosnja_sam >> c._vrsta >> c._naziv >> c._voda >> c._protein >> c._mast
                    >> c._carb >> c._rok >> c._kolicinadan;
            return is;
       }
    protected:
        virtual void printMyself(ostream& os) const
        {
            os << "Naziv: " <<_naziv<< endl;
            os << "Potrosnja jela: " << _potrosnja_sam <<" "<<"kg"<<endl;
        }
};
}

#endif // HUMMUS_H
