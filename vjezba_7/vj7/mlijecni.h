#ifndef MLIJECNI_H
#define MLIJECNI_H
#include "food.h"

using namespace std;

namespace food{
class Mlijecni : public Food
{
    public:
        Mlijecni(double _potrosnja_sam, double _potrosnja_dio, string _vrsta, string _naziv ,
                 double _voda, double _protein, double _mast, double _carb, string _rok, double _kolicinadan);

        friend std::ostream & operator<<(std::ostream &os, const Mlijecni& o){
            o.printMyself(os);
            return os;
        }
        double get_kol(){
            return _potrosnja_sam;
        }

    protected:
        double _potrosnja_sam;
        double _potrosnja_dio;
        virtual void printMyself(std::ostream& os) const{}
};
}

#endif // MLIJECNI_H
