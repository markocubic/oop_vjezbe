#include "vege.h"
#include "food.h"

using namespace std;
using namespace food;

Vege::Vege(double potrosnja_sam, double potrosnja_prilog, string vrsta, string naziv, double voda, double protein,
            double mast, double carb, string rok, double kolicinadan)
            :Food(vrsta, naziv, voda, protein, mast, carb, rok, kolicinadan)
{
    _potrosnja_sam=potrosnja_sam;
    _potrosnja_prilog=potrosnja_prilog;
}
