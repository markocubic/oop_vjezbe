#include "mesni.h"

using namespace std;

Mesni::Mesni(double potrosnja_sam, double potrosnja_dio, string vrsta, string naziv, double voda, double protein,
            double mast, double carb, string rok, double kolicinadan)
            :Food(vrsta, naziv, voda, protein, mast, carb, rok, kolicinadan)
{
    _potrosnja_sam=potrosnja_sam;
    _potrosnja_dio=potrosnja_dio;
}
