#ifndef MESO_H
#define MESO_H

#include <iostream>
#include "mesni.h"

using namespace std;
using namespace food;

namespace food{
class Meso : public Mesni
{
    public:
        Meso(double _potrosnja_sam, double _potrosnja_dio, string _vrsta, string _naziv, double _voda, double _protein,
                 double _mast, double _carb, string _rok, double _kolicinadan);

        friend ostream & operator<<(std::ostream &os, const Meso& c){
            os<<"Naziv je: "<<c._naziv<<"\nPotrosnja jela samostalno: "<<c._potrosnja_sam<<"\n"<<endl;
            return os;
        }
        friend istream & operator>>(istream & is, Meso& c){
            is >> c._potrosnja_sam >> c._potrosnja_dio >> c._vrsta >> c._naziv >> c._voda >> c._protein >> c._mast
                    >> c._carb >> c._rok >> c._kolicinadan;
            return is;
       }
    protected:
        virtual void printMyself(ostream& os) const
        {
            os << "Naziv: " <<_naziv<< endl;
            os << "Potrosnja jela: " << _potrosnja_sam <<" "<<"kg"<<endl;
        }
};
}

#endif // MESO_H
