#ifndef VEGE_H
#define VEGE_H
#include "food.h"

using namespace std;

namespace food{
class Vege : public Food
{
    public:
        Vege(double _potrosnja_sam, double _potrosnja_prilog, string _vrsta, string _naziv ,
             double _voda, double _protein, double _mast, double _carb, string _rok, double _kolicinadan);

        friend std::ostream & operator<<(std::ostream &os, const Vege& o){
            o.printMyself(os);
            return os;
        }
        double get_kol(){
            return _potrosnja_sam;
        }

    protected:
        double _potrosnja_sam;
        double _potrosnja_prilog;
        virtual void printMyself(std::ostream& os) const{}
};
}
#endif // VEGE_H
