#ifndef KREMPITA_H
#define KREMPITA_H

#include "iostream"
#include "kolaci.h"
using namespace std;

namespace food{
class Krempita : public Kolaci
{
    public:
        Krempita(double _potrosnja_sam, string _vrsta, string _naziv, double _voda, double _protein,
                 double _mast, double _carb, string _rok, double _kolicinadan);

        friend ostream & operator<<(std::ostream &os, const Krempita& c){
            os<<"Naziv je: "<<c._naziv<<"\nPotrosnja jela samostalno: "<<c._potrosnja_sam<<"\n"<<endl;
            return os;
        }
        friend istream & operator>>(istream & is, Krempita& c){
            is >> c._potrosnja_sam >> c._vrsta >> c._naziv >> c._voda >> c._protein >> c._mast
                    >> c._carb >> c._rok >> c._kolicinadan;
            return is;
        }

    protected:
        virtual void printMyself(ostream& os) const
        {
            os << "Naziv: " <<_naziv<< endl;
            os << "Potrosnja jela: " << _potrosnja_sam <<" "<<"kg"<<endl;
        }
};
}

#endif // KREMPITA_H
