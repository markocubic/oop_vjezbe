#include <iostream>
#include <vector>

#include "food.h"
    #include "mlijecni.h"
        #include "sir.h"
        #include "mlijeko.h"
        #include "jogurt.h"
    #include "mesni.h"
        #include "meso.h"
        #include "sunka.h"
        #include "prsut.h"
    #include "vege.h"
        #include "riza.h"
        #include "tofu.h"
        #include "hummus.h"
    #include "kolaci.h"
        #include "madjarica.h"
        #include "krempita.h"
        #include "torta.h"

using namespace std;
using namespace food;

void printArtikli(vector<Food> food_arr)
{
    unsigned int n = food_arr.size();
    for (unsigned int i=0; i<n; i++)
    {
        food_arr.at(i).printFood();
    }
}

int main()
{
    vector<Food*> vec;

    Sir sir(8, 2, "sir", "sir", 4.1, 5, 8, 7, "22-08-2023", 19);
    vec.push_back(&sir);

    Mlijeko mli(11, 4, "mlijeko", "mlijeko", 57, 8, 4.8, 1.1, "01-10-2020", 88);
    vec.push_back(&mli);

    Jogurt jog(14, 45, "jogurt", "jogurt", 25, 44, 1, 58, "05-06-2020", 17);
    vec.push_back(&jog);

    Meso mes(2.1, 2, "meso", "meso", 2, 1, 4, 5, "10-11-2023", 22);
    vec.push_back(&mes);
    Sunka sun(2.2, 8, "sunka", "sunka", 4, 5, 7, 8, "21-05-2020", 50);
    vec.push_back(&sun);
    Sunka prs(1.1, 2, "prsut", "sunka", 4, 8, 17, 28, "23-06-2021", 20);
    vec.push_back(&prs);

    Riza riz(4.1, 5.2, "riza", "riza", 4, 2, 7, 4, "19-04-2022", 25);
    vec.push_back(&riz);
    Tofu tof(5.2, 1.1, "tofu", "tofu", 5, 2.1, 4, 7, "25-02-2020", 22);
    vec.push_back(&tof);
    Hummus hum(2.2, 1, "hummus", "hummus", 2, 4, 8, 9, "18-12-2023", 8);
    vec.push_back(&hum);

    Madjarica mad(2.5, "madjarica", "madjarica", 5, 2, 4.7, 1.2, "01-05-2021", 11);
    vec.push_back(&mad);
    Krempita kre(1.4, "jogurt", "jorgut", 1, 2, 4.1, 5, "02-02-2020", 10);
    vec.push_back(&kre);
    Torta tor(2.8, "torta", "torta", 2, 8, 7, 2.2, "05-04-2020", 12);
    vec.push_back(&tor);

    vector<Food*>::iterator it;
    double total=0.0;

    for (int i=0; i<vec.size(); i++){
		cout<<*vec[i]<<endl;
		total=total+vec[i]->get_kol();
	}

    cout<<total<<endl;
}
