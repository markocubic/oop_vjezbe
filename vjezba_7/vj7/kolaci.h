#ifndef KOLACI_H
#define KOLACI_H
#include "food.h"
#include <string>

namespace food{
class Kolaci : public Food
{
    public:
        Kolaci(double _potrosnja_sam, string _vrsta, string _naziv ,double _voda, double _protein, double _mast, double _carb, string _rok, double _kolicinadan);
        friend std::ostream & operator<<(std::ostream &os, const Kolaci& o){
            o.printMyself(os);
            return os;
        }
        double get_kol(){
            return _potrosnja_sam;
        }

    protected:
        double _potrosnja_sam;
        virtual void printMyself(std::ostream& os) const{}
};
}

#endif // KOLACI_H
