#ifndef POTROSNJA_H
#define POTROSNJA_H


class Potrosnja
{
    public:
        void setYear(int _year);
        void setMonth(int _month);
        void setKg(double _kg);
        int getYear() const;
        int getMonth() const;
        double getKg() const;

    private:
        int _year;
        int _month;
        double _kg;
};

#endif // POTROSNJA_H

