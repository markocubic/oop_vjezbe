#include <iostream>
using namespace std;

struct student
{
    string ID;
    string ime;
    string spol;
    int ocjene_kvizova[2];
    int ocjena_sredina, ocjena_kraj, uk_bod, total;
};

void podatci(student *st)
{
    st[0].ID = "111";
    st[0].ime = "Jatele";
    st[0].spol = "musko";
    st[0].ocjene_kvizova[0]=5;
    st[0].ocjene_kvizova[1]=4;
    st[0].ocjena_sredina=3;
    st[0].ocjena_kraj=3;
    st[0].uk_bod = st[0].ocjene_kvizova[0]+st[0].ocjene_kvizova[1];
    st[0].total = 3;

    st[1].ID = "222";
    st[1].ime = "Titele";
    st[1].spol = "zensko";
    st[1].ocjene_kvizova[0]=4;
    st[1].ocjene_kvizova[1]=4;
    st[1].ocjena_sredina=5;
    st[1].ocjena_kraj=4;
    st[1].uk_bod = st[1].ocjene_kvizova[0]+st[1].ocjene_kvizova[1];
    st[1].total = 2;

    st[2].ID = "333";
    st[2].ime = "Matilda";
    st[2].spol = "zensko";
    st[2].ocjene_kvizova[0]=5;
    st[2].ocjene_kvizova[1]=3;
    st[2].ocjena_sredina=3;
    st[2].ocjena_kraj=3;
    st[2].uk_bod = st[2].ocjene_kvizova[0]+st[2].ocjene_kvizova[1];
    st[2].total = 3;

    st[3].ID="000";
}

int findID(student *st, int &br_unosa, string unos_id)
{
    for(int i=0;i<br_unosa;i++)
    {
        if (unos_id == st[i].ID)
            return i;
    }
    return -1;
}

void print_student(student st)
{
    cout << "\n  ID: "<<st.ID<<"\n";
    cout << "  Ime studenta: "<<st.ime<<"\n";
    cout << "  Spol studenta: "<<st.spol<<"\n";
    cout << "  Ocjena kviza 1: "<<st.ocjene_kvizova[0]<<"\n";
    cout << "  Ocjena kviza 2: "<<st.ocjene_kvizova[1]<<"\n";
    cout << "  Ocjena na sredini semestra: "<<st.ocjena_sredina<<"\n";
    cout << "  Ocjena na kraju semestra: "<<st.ocjena_kraj<<"\n";
    cout << "  Ukupan broj bodova: "<<st.uk_bod<<"\n";
    cout << "  Ukupna ocjena: "<<st.total<<"\n\n";
}
void unos_student(student &st)
{
    cout << " \n Unesite Ime studenta: ";
    cin >> st.ime;
    cout << "\n Unesite Spol studenta: ";
    cin >> st.spol;
    cout << "\n Unesite Ocjenu 1. kviza: ";
    cin >> st.ocjene_kvizova[0];
    cout << "\n Unesite Ocjenu 2. kviza: ";
    cin >> st.ocjene_kvizova[1];
    st.uk_bod = st.ocjene_kvizova[0]+st.ocjene_kvizova[1];
    cout << "\n Unesite Ocjenu na sredini semestra: ";
    cin >> st.ocjena_sredina;
    cout << "\n Unesite Ocjenu na kraju semestra: ";
    cin >> st.ocjena_kraj;
    cout << "\n Unesite Ukupnu ocjenu: ";
    cin >> st.total;
}

int prikaz_svih(student *st, int br_unosa, int input)
{
    cout << "\n Prikaz svih zapisa \n\n";

    for(int i=0;i<br_unosa;i++)
        print_student(st[i]);

    cout <<"0 za Izlaz\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    cout <<"\n";
    return input;
}

int prosjek_stud(student *st, int br_unosa, int input)
{
    string unos_id;
    float prosjek=0;
    cout << " \nProsjek bodova za studenta \n";
    cout<<"  Unesite ID studenta: ";
    cin>>unos_id;
    int i=findID(st, br_unosa, unos_id);

    if (i!=-1)
    {
        prosjek = (st[i].ocjene_kvizova[0]+st[i].ocjene_kvizova[1]+st[i].ocjena_sredina+st[i].ocjena_kraj)/4;
        cout<<"   Prosjek bodova studenta "<<st[i].ime<<" je: "<<prosjek<<"\n";
    }
    else if (i==-1)
        cout<<"\nKriv unos ID-a\n";

    cout <<"\n5 za Unjeti novog studenta\n";
    cout <<"0 za Izlaz\n";
    cout <<"Unesite odabir: ";
    cin>>input;
    return input;
}

int najveci_br(student *st, int br_unosa, int input)
{
    int maxbr=0;
    int index;
    for(int i=0;i<br_unosa;i++)
    {
        if(st[i].uk_bod > maxbr)
        {
            maxbr=st[i].uk_bod;
            index=i;
        }
    }
    cout << "\nStudent s najvise bodova ("<<maxbr<<") je "<< st[index].ime;
    print_student(st[index]);
    cout <<"\n0 za Izlaz\n\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    return input;
}

int najmanji_br(student *st, int br_unosa, int input)
{
    int minbr=st[0].uk_bod;
    int index=0;
    for(int i=0;i<br_unosa;i++)
    {
        if(st[i].uk_bod < minbr)
        {
            minbr=st[i].uk_bod;
            index=i;
        }
    }
    cout << "\nStudent s najmanje bodova ("<<minbr<<") je "<< st[index].ime;
    print_student(st[index]);
    cout <<"\n0 za Izlaz\n\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    return input;
}
int student_id(student *st, int br_unosa, int input)
{
    string unos_id;
    cout << "\nPronadi studenta po ID-u\n";
    cout << "  Unesi ID studenta: ";
    cin >> unos_id;
    int i=findID(st, br_unosa, unos_id);

    if(i!=-1)
        print_student(st[i]);
    else
        cout<<"Kriv unos ID-a!\n";

    cout <<"\n8 za ponovni unos ID-a\n";
    cout <<"0 za Izlaz\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    return input;
}

int partitionf(student *st, int *nizi, int start, int endd)
{
    int pivot=endd;
    int pindex=start;
    for (int i=start;i<endd;i++)
    {
        if(st[nizi[i]].uk_bod<=st[nizi[pivot]].uk_bod)
        {
            swap(nizi[i], nizi[pindex]); //swap je funkcija u c++
            pindex++;
        }
    }
    swap(nizi[pindex], nizi[endd]);
    return pindex;
}
void quicksort(student *st, int *nizi, int start, int endd)
{
    if (start<endd)
    {
        int pindex = partitionf(st, nizi, start, endd);
        quicksort(st, nizi, start, pindex-1);
        quicksort(st, nizi, pindex+1, endd);
    }
}
int sort_by_total(student *st, int br_unosa, int input)
{
    int *nizi = new int;
    for (int i=0;i<br_unosa;i++)
        nizi[i]=i;

    int start=0, endd=br_unosa-1;
    quicksort(st, nizi, start, endd);

    cout <<"\nStudenti sortirani po ukupnom broju bodova\n";
    for(int i=0;i<br_unosa;i++)
        print_student(st[nizi[i]]);

    cout <<"\n0 za Izlaz\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    return input;
}

int dodaj_zapis(student *st, int &br_unosa, int input)
{
    string unos_id;
    cout << "\nUnos novog studenta ("<<br_unosa+1<<"):\n";
    cout << "\n Unesite ID studenta: ";
    cin >> unos_id;
    int i=findID(st, br_unosa, unos_id);
    if (i==-1)
    {
        st[br_unosa].ID=unos_id;
        unos_student(st[br_unosa]);

        cout << "\n\n Student " << st[br_unosa].ID<<": \n";
        print_student(st[br_unosa]);

        br_unosa++;
        st[br_unosa].ID="000";
    }
    else
        cout<<"Kriv unos ID-a!\n";

    cout <<"\n0 za Izlaz\n";
    cout <<"1 za ponovni unos ID-a\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    return input;
}

int ukloni_zapis(student *st, int &br_unosa, int input)
{
    string unos_id;
    cout <<"\nBrisanje studenta\n";
    cout <<"\n Unesite ID studenta kojeg zelite izbrisati: ";
    cin >> unos_id;

    int i = findID(st, br_unosa, unos_id);

    if (i!=-1)
    {
        for (int j=i;j<br_unosa;j++)
            st[j] = st[j + 1];

        br_unosa--;
        cout<<"Izbrisan ucenik s ID-om "<<unos_id<<"\n";
    }
    else
        cout<<"Kriv unos ID-a!\n";

    cout <<"\n2 za ponovni unos ID-a\n";
    cout <<"0 za Izlaz\n";
    cout <<"Unesite odabir: ";
    cin >> input;
    return input;
}

int azuriraj_zapis(student *st, int &br_unosa, int input)
{
    string unos_id;
    cout << "\nAzuriranje studenta\n";
    cout << "\n Unesite ID studenta kojeg zelite azurirati: ";
    cin >> unos_id;
    int i=findID(st, br_unosa, unos_id);

    if (i!=-1)
    {
        print_student(st[i]);
        unos_student(st[i]);

        cout << "\n\n Student " << st[i].ID<<": \n";
        print_student(st[i]);
    }
    else
        cout<<"Kriv unos ID-a!\n";

    cout << "\n3 za ponovni unos ID-a\n";
    cout << "0 za Izlaz\n";
    cout << "Unesite odabir: ";
    cin >> input;
    return input;
}

int main()
{
    student st[20];
    podatci(st);
    int input=0, i=0;
    int br_unosa=0;//broj unesenih studenata
    while (st[i].ID != "000")//racuna broj vec unesenih studenata
    {
        i++;
        br_unosa++;
    }
    while(input != 10)
    {
        cout << "\nIzbornik:\n\n";
        cout << " 1 - Dodaj novi zapis\n";
        cout << " 2 - Ukloni zapis\n";
        cout << " 3 - Azuriraj zapis\n";
        cout << " 4 - Prikazi sve zapise\n";
        cout << " 5 - Izracunaj prosjek bodova za studenta\n";
        cout << " 6 - Prikazi studenta s najvecim brojem bodova\n";
        cout << " 7 - Prikazi studenta s najmanjim brojem bodova\n";
        cout << " 8 - Pronadi studenta po ID-u\n";
        cout << " 9 - Sortiraj zapise po broju bodova (total)\n";
        cout << "10 - Izlaz\n\n";

        cout << "Broj unesenih studenata: "<<br_unosa<<"\n";
        cout << "Unesite odabir: ";
        cin >> input;

        while(input==1)
            input=dodaj_zapis(st, br_unosa, input);
        while(input==2)
            input=ukloni_zapis(st, br_unosa, input);
        while(input==3)
            input=azuriraj_zapis(st, br_unosa, input);
        while(input==4)
            input=prikaz_svih(st, br_unosa, input);
        while(input==5)
            input=prosjek_stud(st, br_unosa, input);
        while(input==6)
            input=najveci_br(st, br_unosa, input);
        while(input==7)
            input=najmanji_br(st, br_unosa, input);
        while(input==8)
            input=student_id(st, br_unosa, input);
        while(input==9)
            input=sort_by_total(st, br_unosa, input);
        if(input==10)
            cout<<"\nIzasli ste iz programa";
    }
}
