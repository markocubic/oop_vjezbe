#include <iostream>
#include "food.h"
using namespace std;
int checkIndex(int i, unsigned int n)
{
    while(i>n-1 || i<0)
    {
        cout<<"\n Kriv unos indexa."<<endl;
        cout<<" Pokusajte ponovo: ";
        cin>>i;
    }
    return i;
}
void printArtikli(vector<Food> food_arr)
{
    unsigned int n = food_arr.size();
    for (unsigned int i=0; i<n; i++)
    {
        food_arr.at(i).printFood();
    }
}

vector<Food> mainMenu(vector<Food> food_arr)
{
    unsigned int n = food_arr.size();
    int input=1;
    while (input!=0)
    {
        cout<<"Unesite 0 za izlaz."<<endl;
        cout<<"Unesite 1 za unos artikla."<<endl;
        cout<<"Unesite 2 za printanje odredenog artikla."<<endl;
        cout<<"Unesite 3 za printanje svih artikala."<<endl;
        cout<<"Unesite 4 za printanje godisnje potrosnje odredenog artikla."<<endl;
        cout<<"Unesite 5 za unos potrosnje odredenog artikla za zadani mjesec i godinu."<<endl;
        cout<<"Unesite 6 za mijenjanje potrebne dnevne kolicine artikla."<<endl;
        cout<<"\nUnos: ";

        cin>>input;
        if (input==1)
        {
            n++;
            food_arr.resize(n);
            food_arr.at(n-1).setFood();

            cout<<" Artikal "<<food_arr.at(n-1).getNaziv()<<" unesen.\n"<<endl;
        }
        else if (input==2)
        {
            int i=-1;//mozemo stavit pretrazivanje preko imena artikla
            cout<<"\n Unesite index artikla: ";
            cin>>i;
            i=checkIndex(i, n);

            food_arr.at(i).printFood();
        }
        else if (input==3)
            printArtikli(food_arr);

        else if (input==4)
        {
            int i=-1;
            cout<<"\n Unesite index artikla: ";
            cin>>i;
            i=checkIndex(i, n);
            food_arr.at(i).printPotrosnja();
        }
        else if (input==5)
        {
            int i=-1;
            cout<<"\n Unesite index artikla: ";
            cin>>i;
            i=checkIndex(i, n);
            food_arr.at(i).unosPotrosnje();
        }
        else if (input==6)
        {
            int i=-1;//mozemo stavit pretrazivanje preko imena artikla
            cout<<"\n Unesite index artikla: ";
            cin>>i;
            i=checkIndex(i, n);
            bool flag;
            cout<<"\n Unesite 0 za smanjenje dnevene kolicine artikla za jedan."<<endl;
            cout<<" Unesite 1 za povecanje dnevene kolicine artikla za jedan."<<endl;
            cout<<" Unos: ";
            cin>>flag;
            while (flag != 1 && flag != 0)
            {
                cout<<"\n Kriv unos.";
                cout<<"Pokusajte ponovo: "<<endl;
                cin>>flag;
            }
            food_arr.at(i).dnevnaKolicina(flag);
        }

        else if(input != 0)
            cout<<" Kriv unos. Pokusajte ponovo!"<<endl;
    }
    return food_arr;
}

int main()
{
    vector<Food> food_arr(1); //objekt;

    Food food("mliko", "mlika", 1, 2, 4.1, 5, "20-02-2020", 10);
    food_arr.at(0)=food;



    food_arr=mainMenu(food_arr);
    //food_arr.at(0).printFood();
    Food food1(food_arr.at(0));//copy konstruktor

}
