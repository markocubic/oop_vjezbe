#ifndef FOOD_H
#define FOOD_H
#include <vector>
#include "potrosnja.h"
using namespace std;

class Food
{
    public:
        Food(string _vrsta, string _naziv ,double _voda, double _protein, double _mast, double _carb, string _rok, double _kolicinadan); //konstruktor - odvije se odmah cim kreiramo objekt
        Food();
        Food(const Food& other);
        ~Food();
        string getNaziv() const;
        void setPotrosnja();
        void unosPotrosnje();
        void checkRok();
        void setFood();
        void dnevnaKolicina(bool flag);
        int rokTrajanja();
        void printPotrosnja() const;
        void printFood() const;



    private:
        string _vrsta, _naziv, _rok;
        double _voda, _protein, _mast, _carb, _kolicinadan;
        vector<Potrosnja> _potrosnja;

};

#endif // FOOD_H
