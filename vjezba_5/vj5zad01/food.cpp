#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <vector>
#include <algorithm>
#include "food.h"

using namespace std;

Food::Food()//set defaults
{
    _vrsta = " ";//praksa je da se default stringovi pisu razmakom a ne "null"
    _naziv = " ";
    _voda = 0.0;
    _protein = 0.0;
    _mast = 0.0;
    _carb = 0.0;
    _rok = "null";
    _kolicinadan = 0;
    _potrosnja.resize(12);
    cout<<"\nKonstruktor\n"<<endl;

}

Food::Food(string vrsta, string naziv, double voda, double protein, double mast, double carb, string rok, double kolicinadan)
{
    _vrsta = vrsta;
    _naziv = naziv;
    _voda = voda;
    _protein = protein;
    _mast = mast;
    _carb = carb;
    _rok = rok;
    _kolicinadan = kolicinadan;
}

Food::Food(const Food &other)//copy konstruktor
{
    _vrsta = other._vrsta;
    _naziv = other._naziv;
    _voda = other._voda;
    _protein = other._protein;
    _mast = other._mast;
    _carb = other._carb;
    _rok = other._rok;
    _kolicinadan = other._kolicinadan;
    int br=rokTrajanja()*24;

    _potrosnja.resize(br);
    copy(other._potrosnja.begin(), other._potrosnja.end(), _potrosnja.begin());
}
Food::~Food()//destructor
{
    _potrosnja.clear();
    _potrosnja.shrink_to_fit();
}

string Food::getNaziv() const
{
    return _naziv;
}

void Food::setPotrosnja()
{
    _potrosnja.resize(rokTrajanja()*24);//12*2 - 12 mjeseci udvostruceno
    unsigned int n = _potrosnja.size();
    unsigned int br=0;
    time_t t = time(0);
    tm* now = localtime(&t);
    unsigned int j=1;
    for (unsigned int i=0; i<n; i++)
    {
        _potrosnja.at(i).setYear(now->tm_year+1900 + br);
        _potrosnja.at(i).setMonth(j);

        if(j%12==0){
            br++;
            j=j/12;
        }
        else
            j++;
    }
}

void Food::unosPotrosnje()
{
    unsigned int n = _potrosnja.size();
    unsigned int y, m;
    while(true)
    {
        cout<<"\n Unesite godinu: ";
        cin>>y;
        cout<<"\n Unesite mjesec: ";
        cin>>m;
        for(unsigned int i=0; i<n; i++)
        {
            if (_potrosnja.at(i).getYear()==y && _potrosnja.at(i).getMonth()==m)
            {
                if(_potrosnja.at(i).getKg() != 0)
                    cout<<" Potrosnja za mjesec: "<<_potrosnja.at(i).getMonth()<<" i godinu: "<<" je vec unesena!"<<endl;

                else{
                    double kg;
                    cout<<"Unesite potrosnju za ovaj mjesec: ";
                    cin>>kg;
                    _potrosnja.at(i).setKg(kg);
                    return;
                }
            }
        }
        cout<<" Takav datum ne postoji za artikal "<<_naziv<<"."<<endl;
        cout<<" Pokusajte ponovo: ";
    }
}

void Food::checkRok()
{
    string rokGod = _rok.substr(6, 4); //Godina roka trajanja;
    int intRokGod = atoi(rokGod.c_str());

    time_t t = time(0);
    tm* now = localtime(&t);
    int trenutnaGod=now->tm_year+1900;

    while (true)
    {
        if (_rok[2]!='-' || _rok[5]!='-' || intRokGod<trenutnaGod)
        {
            cout<<"  Kriv unos. Pokusajte ponovo(dd-mm-yyyy): ";//datum mora biti unesen kako treba
            cin>>_rok;
        }
        else
            return;
    }
}

void Food::setFood()
{
    cout<<" Unesite namirnicu: \n"<<endl;
        cout<<"  Vrsta artikla: ";
        cin>>_vrsta;
        cout<<endl;
        cout<<"  Ime artikla: ";
        cin>>_naziv;
        cout<<endl;
        cout<<"  Kolicina vode(100g): ";
        cin>>_voda;
        cout<<endl;
        cout<<"  Kolicina proteina(100g): ";
        cin>>_protein;
        cout<<endl;
        cout<<"  Kolicina masti(100g): ";
        cin>>_mast;
        cout<<endl;
        cout<<"  Kolicina ugljikohidrata(100g): ";
        cin>>_carb;
        cout<<endl;
        cout<<"  Rok trajanja(dd-mm-yyyy): "; //datum mora biti unesen kako treba
        cin>>_rok;
        checkRok();
        cout<<endl;
        cout<<"  Dnevna potrebna kolicina hrane(kg): ";
        cin>>_kolicinadan;
        cout<<endl;
        setPotrosnja();
}

void Food::dnevnaKolicina(bool flag)
{
    if (flag==1){
        _kolicinadan++;
        cout<<"\n Dnevna potrebna kolicina hrane(kg) artikla "<<_naziv<<" povecana za 1.\n"<<endl;
    }
    if (flag==0){
        _kolicinadan--;
        cout<<"\n Dnevna potrebna kolicina hrane(kg) artikla "<<_naziv<<" smanjena za 1.\n"<<endl;
    }
}

int Food::rokTrajanja()
{
    string rokGod;
    rokGod = _rok.substr(6, 4); //Godina roka trajanja

    int intRokGod = atoi(rokGod.c_str());
    time_t t = time(0);
    tm* now = localtime(&t); //Trenutna godina
    int razlika = intRokGod - (now->tm_year+1900);

    return razlika;
}

void Food::printPotrosnja() const
{
    int n = _potrosnja.size();
    cout<<" Potrosnja artikla "<<"'"<<_naziv<<"'"<<":"<<endl;
    for ( int i=0; i<n; i++)
    {
        cout<<"  Godina: "<<_potrosnja.at(i).getYear()<<" | "<<"Mjesec: "<<_potrosnja.at(i).getMonth();
        (_potrosnja.at(i).getMonth()<10) ? (cout<<"  | ") : (cout<<" | "); //Razmak u slučaju kad broj mjeseca ima dvije znamenke. Estetski razlozi.
        cout<<"Kilogrami: "<<_potrosnja.at(i).getKg()<<endl;
    }
}

void Food::printFood() const
{
    cout<<"\n Vrsta je: "<<_vrsta<<endl;
    cout<<" Naziv je: "<<_naziv<<endl;
    cout<<" Voda(100g): "<<_voda<<endl;
    cout<<" Protein(100g): "<<_protein<<endl;
    cout<<" Mast(100g): "<<_mast<<endl;
    cout<<" Ugljikohidrati(100g): "<<_carb<<endl;
    cout<<" Rok je: "<<_rok<<endl;
    cout<<" Dnevna potrebna kolicina je: "<<_kolicinadan<<endl;
    cout<<"\n";
}


