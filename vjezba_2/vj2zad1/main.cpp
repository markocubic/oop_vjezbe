#include <iostream>

using namespace std;

int partitionf(int *niz, int start, int endd)
{
    int pivot=niz[endd];
    int pindex=start;
    for (int i=start;i<endd;i++)
    {
        if(niz[i]<=pivot)
        {
            swap(niz[i], niz[pindex]);
            pindex++;
        }
    }
    swap(niz[pindex], niz[endd]);
    return pindex;
}
int quicksort(int *niz, int start, int endd)
{
    if (start<endd)
    {
        int pindex = partitionf(niz, start, endd);
        quicksort(niz, start, pindex-1);
        quicksort(niz, pindex+1, endd);
    }
}

void funkcija(int *niz)
{
    int flag=0, br=0;
    int *missing = new int[9];
    int nizi[]={1,2,3,4,5,6,7,8,9};//nemora nuzno bit od 1 do 9
    for(int i=0;i<9;i++)
    {
        for(int j=0;j<9;j++)
        {
            if(nizi[i]==niz[j])
                flag=1;
        }
        if (flag==0)
        {
            missing[br]=nizi[i];
            br++;
        }
        flag=0;
    }

    int *missingn = new int[br];
    for (int i=0;i<br;i++)
        missingn[i]=missing[i];
    delete [] missing;
    missing = 0;

    cout<<"Brojevi koji fale: ";
    for (int i=0;i<br;i++)
        cout<<missingn[i]<<" ";
    cout<<"\n";
    quicksort(niz, 0, 8);

    for (int i=0;i<br;i++)
    {
        for(int j=0;j<9;j++)
        {
            if (niz[j]<1 || niz[j]>9)
            {
                niz[j]=missingn[i];
                break;
            }

            else if(niz[j]==niz[j+1])
            {
                niz[j]=missingn[i];
                break;
            }
        }
    }
    quicksort(niz, 0, 8);

}
int main()
{
    int *niz = new int[9];
    cout<<"Unosi brojeve od 1 do 9:";
    for(int i=0;i<9;i++)
        cin>>niz[i];

    funkcija(niz);

    for(int i=0;i<9;i++)
        cout<<"\n"<<niz[i]<<" ";
    delete [] niz;
    niz=0;
}
