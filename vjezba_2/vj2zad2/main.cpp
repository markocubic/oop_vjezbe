#include <iostream>
using namespace std;
void funkcija(int *niz)
{
    for(int i=0;i<8;i++)
    {
        if(niz[i]%2!=0)
        {
            for(int j=i+1;j<8;j++)
            {
                if(niz[j]%2==0)
                {
                    swap(niz[i], niz[j]);
                    break;
                }
            }
        }
    }
}
int main()
{
    int *niz = new int [8];
    cout<<"Unosi brojeve: \n";
    for (int i=0;i<8;i++)
        cin>>niz[i];
    cout<<"\n";

    funkcija(niz);
    for (int i=0;i<8;i++)
        cout<<niz[i]<<" ";

    delete [] niz;

}
