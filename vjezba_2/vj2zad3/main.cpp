#include <iostream>
#include <stdlib.h>//malloc, realloc, free

using namespace std;
struct vektor{
    int fiz;//fizicka velicina (max broj mogucih elemenata u vektoru)
    int *niz;
    int log;//logicka velicina (trenutan broj elemenata u vektoru)

    void vector_new(vektor v)//nebi trebalo vracat strukturu
    {
        fiz=3;
        niz=new int[fiz];
        log=0;
    }

    void vector_delete(vektor v)
    {
        delete [] niz;
        fiz=0;
        log=0;
    }

    void vector_push_back(vektor v, int element)
    {
        niz[log]=element;
        log++;
    }

    void vector_pop_back(vektor v)
    {
        if (log != 0)
        {
            niz[log-1]=0;
            log--;
        }
        else
            cout<<"\nVektor je prazan";
    }

    void vector_front(vektor v, int element)
    {
        niz[0]=element;
    }

    void vector_back(vektor v, int element)
    {
        niz[log]=element;
    }

    int vector_size(vektor v)
    {
        return log;
    }
};

void copy_arr(int *niz2, int *niz1, int siz)
{
    for (int i=0;i<siz;i++)
        niz2[i]=niz1[i];
}
vektor nvector(vektor v)
{
    v.vector_new(v);
    int *niz1 = (int*) malloc(v.fiz*sizeof(int));
    cout<<"Unesi brojeve: \n";
    for(int i=0;i<6;i++)
    {
        if(i==v.fiz-1)
        {
            v.fiz*=2;
            niz1=(int*)realloc(niz1, sizeof(int)*v.fiz);
        }

        cin>>niz1[i];
        v.log++;
    }
    if(v.log<v.fiz)
    {
        for(int i=v.log;i<v.fiz;i++)
            niz1[i]=0;
    }
    v.niz=new int[v.fiz];
    for(int i=0;i<v.fiz;i++)
        v.niz[i]=niz1[i];
    return v;
}
int main()
{
    vektor v;

    v=nvector(v);
    for(int i=0;i<v.fiz;i++)
        cout<<v.niz[i]<<" ";
    cout<<"\n";

    v.vector_push_back(v, 99);//Adds a new element at the end of the vector, after its current last element.
    for(int i=0;i<v.fiz;i++)
        cout<<v.niz[i]<<" ";
    cout<<"\n";

    v.vector_pop_back(v);//Removes the last element in the vector, effectively reducing the container size by one.
    for(int i=0;i<v.fiz;i++)
        cout<<v.niz[i]<<" ";
    cout<<"\n";

    v.vector_front(v, 88);//Returns a reference to the first element in the vector. Profesor reka da ne koristimo return pa ovo nije tocno
    for(int i=0;i<v.fiz;i++)
        cout<<v.niz[i]<<" ";
    cout<<"\n";

    v.vector_back(v, 91);//Returns a reference to the last element in the vector.
    for(int i=0;i<v.fiz;i++)
        cout<<v.niz[i]<<" ";
    cout<<"\n";

    int br=v.vector_size(v);
    cout<<br;

    v.vector_delete(v);
}
